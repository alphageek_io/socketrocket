<?php 

     /** 
     * Super simple log handler
     * 
     * @author Kevin Elshout <kevin@alphageek.io>
     * @copyright Kevin Elshout - 2014
     * @version 1.0
     *
     */
     

    class LogHandler {
                
        function write($message) {
            $logFile  = ROOT . '/logs/log.txt';
            $logEntry = sprintf(
                '[%s] %s',
                date('Y-m-d H:i:s'), $message
            );
            file_put_contents(
                $logFile, $logEntry . "\n", FILE_APPEND
            );
        }
        
    }