<?php

    /** 
     * Socket functions wrapper
     * 
     * @author Kevin Elshout <kevin@alphageek.io>
     * @copyright Kevin Elshout - 2014
     * @version 1.0
     *
     */
     
    class SocketWrapper {
        
        /** 
         * Creates socket and returns resource
         * 
         * @param int $domain , int $type , int $protocol
         * @return resource
         */
 
        function create($domain = AF_INET, $type = SOCK_STREAM, $protocol = SOL_TCP) {
            return socket_create($domain, $type, $protocol);
        }
        
        /** 
         * Set socket option
         * 
         * @param resource $socket , int $level , int $optname , mixed $optval
         * @return boolean
         */
        
        function setOption($socket, $level, $optname, $optval) {
            return socket_set_option($socket, $level, $optname, $optval);
        }
        
        /** 
         * Bind socket to address and port
         * 
         * @param resource $socket , string $address [, int $port = 0 ]
         * @return boolean
         */
        
        function bind($socket, $address = 0, $port = 110) {
            return socket_bind($socket, $address, $port);
        }
        
        /** 
         * Make socket listen for connections
         * 
         * @param resource $socket [, int $backlog = 0 ]
         * @return boolean
         */        
        
        function listen($socket) {
            return socket_listen($socket);
        }
        
        /** 
         * Make socket accept new connections
         * 
         * @param  resource $socket 
         * @return boolean
         */  
                       
        function accept($socket) {
            return socket_accept($socket);
        }
        
        /** 
         * Write data to socket
         * 
         * @param  resource $socket, string $message
         * @return boolean
         */ 
        
        function write($socket, $message) {
            return socket_write($socket, $message);
        }
        
        /** 
         * Gets peername for socket
         * 
         * @param  resource $socket , string &$address [, int &$port ]
         * @return string
         */ 
         
        function getPeerName($socket, $address) {
            socket_getpeername($socket, $address);
            return $address;
        }
    }