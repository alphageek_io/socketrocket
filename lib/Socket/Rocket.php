<?php

    /** 
     * SocketRocket-class
     * 
     * This class contains all serverlogic and methods for the SocketRocket application
     *
     * @author Kevin Elshout <kevin@alphageek.io>
     * @copyright Kevin Elshout - 2014
     * @version 1.0
     *
     */

    class SocketRocket {
    
        function __construct() {
            LogHandler::write('Starting server');
        }
        
        /** 
         * Creates and configures the socket for the server
         * You can change the address and port in lib/config.php
         * 
         * @param array $config
         * @return resource
         */
        
        function createSocket($config = null) {
            
            $socket = SocketWrapper::create();
            # Configure socket to reuse adress
            SocketWrapper::setOption($socket, SOL_SOCKET, SO_REUSEADDR, 1);
            # Bind socket
            SocketWrapper::bind($socket, $config['address'], $config['port']);
            LogHandler::write(
                'Server now listening to ' . $config['address'] . ':' . $config['port']
            );
            # Make socket listen
            SocketWrapper::listen($socket);                

            # Return socket
            return $socket;
        }
        
        /** 
         * Checks for new connections, accepts new clients 
         * and adds the socket to the clientlist.
         * Greets new connections with welcome message and instructions
         * 
         * @param resource $socket, array $currentClientList
         * @return array $currentClientList
         */
        
        function checkForNewClients($socket, $currentClientList) {
            
            # Do we have new users?
            if (in_array($socket, $currentClientList)) {
               
                # Accepts new client
                $this->clientList[] = $newSocket = SocketWrapper::accept($socket);                    
                # Count clients
                $numberOfClients = count($this->clientList) - 1;
                
                # Format welcome-message
                $message = 
                    "Welkom op de SocketRocket-chatserver! \n" .
                    "Er zijn {$numberOfClients} gebruikers verbonden met de server. \n\n" .
                    "Beschikbare commando's: \n" .
                    "/quit - Verlaat de chatserver \n" .
                    "/nick NICKNAME - Verander je nickname \n\n" . 
                    "Vul je nickname in en druk op enter \n\n";
                
                # Greet user
                SocketWrapper::write($newSocket, $message);
                
                # Gets the name (ip) of peer
                $ip = SocketWrapper::getPeerName($newSocket, $ip);
                LogHandler::write(
                    "Accepted incoming connection from peer: {$ip}" 
                );

                # Add new socket and userinfo 
                $this->currentClient = array_search($newSocket, $this->clientList);
                $this->clientReference[$this->currentClient] = false;

                # Remove listening-socket from clientlist
                $this->serverSocket = array_search($socket, $currentClientList);                    
                unset($currentClientList[$this->serverSocket]);
            }
            return $currentClientList;
        }
        
        /** 
         * Handles the commands supported by the server:
         * - Update / set nickname
         * - Disconnect from server
         *
         * @param string $data, resource $socket, resource $receiverSocket
         * @return none;
         */
        
        function handleCommand($data, $socket, $receiverSocket) {
            $arguments = explode(' ', substr($data, 1));
            list($command, $value) = $arguments;
            switch (true) {
                case $command == 'nick': 
                    $this->handleNickname($socket, $receiverSocket, $value);
                    break;
                case $command == 'quit': 
                    foreach ($this->clientList as $senderSocket) {
                        if ($senderSocket != $socket && $senderSocket == $receiverSocket) {
                            $username = $this->clientReference[$clientKey];
                            SocketWrapper::write($senderSocket, "Bye \n");
                            LogHandler::write(
                                "User {$username} disconnected"
                            );
                            socket_close($senderSocket);
                            $clientKey = array_search($senderSocket, $this->clientList);
                            unset($this->clientList[$clientKey]);
                            unset($this->clientReference[$clientKey]);
                        }
                    }
                    break;
            } 
        }
        
        /** 
         * Handles the sending of messages to connected users
         *
         * @param resource $socket, resource $recieverSocket, string $data
         * @return none
         */
        
        function handleMessage($socket, $receiverSocket, $data) {
            foreach ($this->clientList as $senderSocket) {
                # Is this the same socket as the message-sender? Then skip it
                if ($senderSocket == $socket || $senderSocket == $receiverSocket) {
                    continue;
                }
                
                $username = $this->clientReference[$this->currentClient];
                # Format message
                $userMessage = sprintf(
                    "%s [%s] %s \n",
                    date('H:i:s'), $username, $data
                );
                # Send it
                SocketWrapper::write($senderSocket, $userMessage);
                LogHandler::write(
                    'Message: ' . $data . ' - by ' . $username
                );
            }
        }
        
        /** 
         * Handles the updating of nicknames
         *
         * @param resource $socket, resource $recieverSocket, string $data
         * @return none
         */
        
        function handleNickname($socket, $receiverSocket, $data) {
            # Is nickname free?
            $oldUsername = $this->clientReference[$this->currentClient];
            if (!in_array($data, $this->clientReference)) {
                $this->clientReference[$this->currentClient] = $data;
                $userMessage = sprintf(
                    "Je nickname is %s! Lets go-deo! \n\n",
                    $data
                );                
                LogHandler::write(
                    "User {$oldUsername} changed nickname to {$data}"
                );
            } 
            else {
                $userMessage = sprintf(
                    "De nickname %s is al in gebruik :-( \n\n",
                    $data
                );   
            }
            # Report update to the user that requested it
            foreach ($this->clientList as $senderSocket) {
                if ($senderSocket == $receiverSocket) {
                    SocketWrapper::write($senderSocket, $userMessage);
                }
            }
        }
        
        /** 
         * Handles the connections and commands / messages sent by our clients (continous loop)
         *
         * @param resource $socket
         * @return none
         */
        
        function handleConnections($socket) {
                        
            # Make a list of all clients and add listening socket
            $this->clientList      = array($socket);
            
            # Create store for nicknames
            $this->clientReference = array();
            
            while (true) {
                
                # Make a copy of the clientlist, because socket_select modifies it
                $currentClientList = $this->clientList;
                                
                # Do we have new data or clients? If not, skip to next while-iteration
                if (@socket_select($currentClientList, $write = NULL, $except = NULL, 0) < 1) {
                    continue;
                }
                
                $currentClientList = $this->checkForNewClients($socket, $currentClientList);
                                
                # Check every client for new data
                foreach ($currentClientList as $receiverSocket) {
                
                    # Read data, either up to 1024 bytes or a newline
                    $data = @socket_read($receiverSocket, 1024, PHP_NORMAL_READ);
                    
                    # Who is it?
                    $this->currentClient = array_search($receiverSocket, $this->clientList);
                    # Client still connected?
                    if ($data === false) {
                        # Disconnected client, find clientkey and remove the client
                        unset($this->clientList[$this->currentClient]);
                        unset($this->clientReference[$this->currentClient]);
                        # Report to server log                        
                        LogHandler::write(
                            "User {$username} disconnected"
                        );
                        # Onto the next client
                        continue;
                    } 
                    
                    # Strip whitespace (because a single space is not a message)
                    $data = trim($data);
                    
                    # Do we have a message or a command?
                    if (substr($data, 0, 1) == '/') {
                        $this->handleCommand($data, $socket, $receiverSocket);
                    } 
                    # No command, handle message
                    else 
                    {
                        # Only send if user has a nickanem
                        if (!empty($data) && $this->clientReference[$this->currentClient] != false) {
                            $this->handleMessage($socket, $receiverSocket, $data);
                        } elseif (!empty($data) && $this->clientReference[$this->currentClient] == false) {
                            $this->handleNickname($socket, $receiverSocket, $data);
                        }
                    }
                }
            }
        }
    }
