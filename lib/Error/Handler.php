<?php

     /** 
     * Super simple error handler handler
     * 
     * @author Kevin Elshout <kevin@alphageek.io>
     * @copyright Kevin Elshout - 2014
     * @version 1.0
     *
     */

    function ErrorHandler($errno, $errstr, $errfile, $errline) {
        
        # Not in error_reporting? Then don't show it now as well
        if (!(error_reporting() & $errno)) {
            return;
        }
    
        # Handle default errortypes
        switch ($errno) {
            case E_USER_ERROR:
                
                $message = sprintf(
                    "<b>Error</b> [%s] %s - Fatal error on line %d in file %s \n 
                     Aborting execution",
                     $errno, $errstr, $errline, $errfile
                );
                Log::write($message);
                exit(1);
                break;
        
            case E_USER_WARNING:
                $message = sprintf(
                    "<b>Warning</b> [%s] %s \n
                     Problem exists on line %d in file %s \n
                     Continuing execution",
                     $errno, $errstr, $errline, $errfile
                );
                Log::write($message);
                break;
        
            case E_USER_NOTICE:
                $message = sprintf(
                    "<b>Notice</b> [%s] %s \n
                     Problem exists on line %d in file %s \n
                     Continuing execution",
                     $errno, $errstr, $errline, $errfile
                );
                Log::write($message);
                break;
        
            default:
                echo "Unknown error type: [$errno] $errstr \n";
                break;
            }
    
        # Don't trigger PHP's error handler
        return true;
    }