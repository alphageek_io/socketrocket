<?php

    /** 
     * SocketRocket
     * 
     * SocketRocket is a PHP5-based OO-chatserver, which uses sockets for communication
     * Telnet is used a client to communicate with the other users
     * 
     * The server can be started in two ways
     * 1. Directly via the PHP-cli with the command 'php -q server.php'
     * 2. As a daemon with the command './start_server'
     *
     * @author Kevin Elshout <kevin@alphageek.io>
     * @copyright Kevin Elshout - 2014
     * @version 1.0
     *
     */

    define('ROOT', getcwd());

    require_once("lib/Error/Handler.php");
    require_once("lib/Log/Handler.php");
    require_once("lib/Socket/Wrapper.php");
    require_once("lib/Socket/Rocket.php");
    require_once("lib/config.php");
            
    $server = new SocketRocket();
    $socket = $server->createSocket($config);
    $server->handleConnections($socket);
    
            
    
        
            
