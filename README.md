**SocketRocket**

  SocketRocket is a PHP5-based OO-chatserver, which uses sockets for communication.  
  Telnet is used as a client to communicate with the other users

  The server can be started in two ways

  1. Directly via the PHP-cli with the command 'php server.php'  
  2. As a background process with the command './start_server'  
  By default the server runs at localhost:9999, but this can be changed in lib/config.php

@author     Kevin Elshout <kevin@alphageek.io>  
@copyright  Kevin Elshout - 2014  
@version    1.0  
